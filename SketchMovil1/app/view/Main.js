Ext.define('Test.view.Main', {
    extend: 'Ext.tab.Panel',
    xtype: 'main',
    requires: [
        'Ext.TitleBar',
    ],
    config: {
        tabBarPosition: 'bottom',

        items: [
            {
                title: 'Inicio',
                scrollable: true,
                iconCls: 'home',
                items: [{
                    docked: 'top',
                    xtype: 'titlebar',
                    title: '',
                    items: [
                        {
                            text: 'Inicio',
                            iconMask: true,
                            align: 'left'
                        },{
                            iconCls:'user',
                            iconMask: true,
                            align: 'right'
                        },
                        {
                            iconCls: 'bookmarks',
                            iconMask: true,
                            align: 'right'
                        },
                        {
                            iconCls: 'compose',
                            iconMask: true,
                            align: 'right'
                        },
                        {
                            iconCls: 'info',
                            iconMask: true,
                            align: 'right'
                        }
                    ]
                },{
                    xtype: 'panel',
                    items:[{
                        xtype: 'image',
                        margin: '30 0 0 0',
                        width: '100%',
                        height: 50,
                        src: 'resources/iconos/logoInegiBanner.png'
                    }]
                },{
                    xtype: 'fieldset',
                    title: '&iquestQu&eacute; est&aacute; buscando?',
                    margin: '30 0 0 0',
                    items: [
                        {
                            xtype: 'searchfield',
                            label: '',
                            name: 'query'
                        }
                    ]
                },{
                    xtype: 'button',
                    margin: '30 0 0 0',
                    text: 'Estad&iacute;stica',
                    height: 50
                },{
                    xtype: 'button',
                    margin: '5 0 0 0',
                    text: 'Geograf&iacute;a',
                    height: 50
                },{
                    xtype: 'button',
                    margin: '5 0 0 0',
                    text: 'Productos y otros servicios',
                    height: 50
                },{
                    xtype: 'button',
                    margin: '5 0 0 0',
                    text: 'Acerca del INEGI',
                    height: 50
                },{
                    xtype: 'list',
                    itemTpl: '{name}',
                    store: {
                         fields: ['name'],
                         data: [
                            {name: 'Cowper'},
                            {name: 'Everett'},
                            {name: 'University'},
                            {name: 'Forest'}
                        ]
                    },
                },{
                    xtype: 'fieldset',
                    title: 'CENTROS DE INFORMACI&Oacute;N',
                    items: [
                        {
                            xtype: 'selectfield',
                            label: '',
                            options: [
                                {text: 'Aguascalientes',  value: '1'},
                                {text: 'Baja California Norte', value: '2'},
                                {text: 'Baja California Sur',  value: '3'},
                                {text: 'Campeche',  value: '4'}
                            ]
                        }
                    ]
                },{
                    xtype: 'label',
                    html: 'M&eacute;xico en Cifras',
                    margin: '0 0 0 10',
                    style: {
                        'font-size':'25px',
                        'font-weight':'bold',
                        'color': 'black'
                    }
                },{
                    xtype: 'label',
                    html: 'Informaci&oacute;n Nacional<br>por Entidades Federativas<br>y Municipios',
                    margin: '0 0 0 10',
                    style: {
                        'font-size':'18px',
                        'font-weight':'bold',
                        'color': 'black'
                    }
                },{
                    xtype: 'fieldset',
                    title: '',
                    labelWrap: true,
                    items: [
                        {
                            xtype: 'selectfield',
                            label: '',
                            options: [
                                {text: 'Aguascalientes',  value: '1'},
                                {text: 'Baja California Norte', value: '2'},
                                {text: 'Baja California Sur',  value: '3'},
                                {text: 'Campeche',  value: '4'}
                            ]
                        }
                    ]
                },{
                    xtype: 'panel',
                    items:[{
                        xtype: 'image',
                        margin: '30 0 0 10',
                        width: '100%',
                        height: 150,
                        src: 'resources/iconos/mapa.png'
                    }]
                },{
                    xtype: 'label',
                    html: 'Censo de pobalci&oacute;n y vivienda',
                    margin: '10 0 0 10',
                    style: {
                        'font-size':'18px',
                        'font-weight':'bold',
                        'color': 'black'
                    }
                },{
                    xtype: 'fieldset',
                    title: '',
                    items: [
                        {
                            xtype: 'selectfield',
                            label: '',
                            options: [
                                {text: 'Estados Unidos Mexicanos',  value: '1'}
    
                            ]
                        },{
                            xtype: 'selectfield',
                            label: '',
                            options: [
                                {text: 'Municipio',  value: '1'}
    
                            ]
                        }
                    ]
                },{
                    xtype: 'label',
                    html: 'Hombres: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 54,855,23',
                    margin: '0 0 0 10',
                    style: {
                        'font-size':'18px',
                        'font-weight':'bold',
                        'color': 'black'
                    }
                },{
                    xtype: 'label',
                    html: 'Mujeres: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 57,481,307',
                    margin: '0 0 0 10',
                    style: {
                        'font-size':'18px',
                        'font-weight':'bold',
                        'color': 'black'
                    }
                },{
                    xtype: 'label',
                    html: 'Total: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 112,336,538',
                    margin: '0 0 0 10',
                    style: {
                        'font-size':'18px',
                        'font-weight':'bold',
                        'color': 'black'
                    }
                },{
                    xtype: 'label',
                    html: 'NOTICIAS DE INTER&Eacute;S',
                    margin: '20 0 0 10',
                    style: {
                        'font-size':'25px',
                        'font-weight':'bold',
                        'color': 'black'
                    }
                },{
                    xtype: 'label',
                    html: '12 de Enero',
                    margin: '10 0 0 10',
                    style: {
                        'font-size':'18px',
                        'font-weight':'bold',
                        'color': 'black'
                    }
                },{
                    xtype: 'label',
                    html: 'SE RENUEVA EL ACUERDO DE <br> COOPERACION ENTRE EL INEGI<br> Y LA UNODC',
                    margin: '10 0 0 10',
                    style: {
                        'font-size':'18px',
                        'font-weight':'bold',
                        'color': 'black'
                    }
                },{
                    xtype: 'label',
                    html: '<a href="">+ver nota completa<a/>',
                    margin: '10 0 0 0',
                    style: {
                        'font-size':'18px',
                        'font-weight':'bold',
                        'color': 'black',
                        'text-align': 'right'
                    }
                },{
                    xtype: 'label',
                    html: 'ESTADISTICAS A PROPOSITO <br>DEL DIA MUNDIAL DE LA<br> DIABETES',
                    margin: '10 0 0 10',
                    style: {
                        'font-size':'18px',
                        'font-weight':'bold',
                        'color': 'black'
                    }
                },{
                    xtype: 'label',
                    html: '<a href="">+ver nota completa<a/>',
                    margin: '10 0 0 0',
                    style: {
                        'font-size':'18px',
                        'font-weight':'bold',
                        'color': 'black',
                        'text-align': 'right'
                    }
                },{
                    xtype: 'label',
                    html: 'INFORMACION OPORTUNA<br>SOBRE LA ACTIVIDAD INDUSTRIAL<br>EN MEXICO',
                    margin: '10 0 0 10',
                    style: {
                        'font-size':'18px',
                        'font-weight':'bold',
                        'color': 'black',
                    }
                },{
                    xtype: 'label',
                    html: '<a href="#">+ver nota completa<a/>',
                    margin: '10 0 0 0',
                    style: {
                        'font-size':'18px',
                        'font-weight':'bold',
                        'color': 'black',
                        'text-align': 'right'
                    }
                },{
                    xtype: 'label',
                    html: '27 de Diciembre',
                    margin: '10 0 0 10',
                    style: {
                        'font-size':'18px',
                        'font-weight':'bold',
                        'color': 'black'
                    }
                },{
                    xtype: 'label',
                    html: 'INDICADORES OPORTUNOS<br>DE OCUPACION Y EMPLEO',
                    margin: '10 0 0 10',
                    style: {
                        'font-size':'18px',
                        'font-weight':'bold',
                        'color': 'black',
                    }
                },{
                    xtype: 'label',
                    html: '<a href="#">+ver nota completa<a/>',
                    margin: '10 0 0 0',
                    style: {
                        'font-size':'18px',
                        'font-weight':'bold',
                        'color': 'black',
                        'text-align': 'right'
                    }
                }
                ], //Termina el contenido de INICIO
            }// , Agregar otro view
        ]
    }
});
